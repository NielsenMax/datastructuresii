module Par ((|||)) where

import Control.Parallel

infix 1 |||

(|||)   ::   a -> b -> (a,b)
a ||| b = a `par` b `par` (a,b)

data BTree a = Empty | Node Int (BTree a) a (BTree a) deriving Show

tree = (Node 3 (Node 2 Empty 1 (Node 1 (Node 0 Empty 7 Empty) 6 Empty)) 2 (Node 1 (Node 0 Empty 3 Empty) 4 (Node 0 Empty 5 Empty)))

inorder Empty = ["e"]
inorder (Node _ l b r) =  (show b) :  (inorder l ++ inorder r)

-- O(h) with h the heigth of the element
nth :: BTree a -> Int -> a
nth (Node n l b r) i | i == n = b
                     | i < n = nth l i
                     | otherwise = nth r (i - n - 1)
-- O(left heigth)
cons a Empty = Node 0 Empty a Empty
cons a (Node n l b r) = Node (n+1) (cons a l) b r

-- W = O(n), S = O(h)
mapBT f Empty = Empty
mapBT f (Node n l a r) = let (l', r') = mapBT f l ||| mapBT f r
                         in Node n l' (f a) r'

-- O(i)
takeBT :: Int -> BTree a -> BTree a
takeBT i Empty = Empty
takeBT i (Node n l b r) | (i - 1) == n = Node n l b Empty
                        | (i - 1) < n = takeBT i l
                        | otherwise = Node n l b $ takeBT (i - n - 1) r
-- O(i)
dropBT i Empty = Empty
dropBT i (Node n l b r) | (i - 1) == n = r 
                        | (i - 1) < n = Node n (dropBT i l)b r
                        | otherwise = dropBT (i - n - 1) r

-- 2

data Tree a = E | Leaf a | Join (Tree a) (Tree a) deriving Show

tree_mcss = Join (Join (Join (Leaf 1) (Leaf (-5))) (Join (Leaf 2) (Leaf (-1)))) (Leaf 3)

mapreduce f g e = mr
  where mr E = e
        mr (Leaf a) = f a
        mr (Join l r) = let (l', r') = mr l ||| mr r
                        in g l' r'

mcss t = let (m',p',s',t') = mapreduce base combine val t
         in m'
 where val = (0,0,0,0)
       base v = let v' = max v 0
                in (v',v',v',v)
       combine (m, p, s, t) (m', p', s', t') = (max (s+p') (max m m'), max p (t+p'), max s' (s+t'), t + t')
-- 3

reduceT f e E = e
reduceT f e (Leaf a) = a
reduceT f e (Join l r) = let (l', r') = reduceT f e l ||| reduceT f e r
                         in f l' r'

sufijos t = fst $ s t t
  where s t E = (E, E)
        s t (Leaf _)   = let t' = dropFirst t 
                         in (t', t')
        s t (Join l r) = let (l', t') = s t l
                             (r', t'') = s t' r
                         in (Join l' r', t'')
        dropFirst E = E
        dropFirst (Leaf _) = E
        dropFirst (Join (Leaf _) (Leaf a)) = Leaf a
        dropFirst (Join l r) = Join (dropFirst l) r

conSufijos t = cS t $ sufijos t
  where cS E _ = E
        cS (Leaf a) t = Leaf (a, t)
        cS (Join l r) (Join l' r') = Join (cS l l') (cS r r')

maxT = reduceT max 0

maxAll = mapreduce (\(a, t) -> maxT t - a) max 0

mejorGanancia = maxAll . conSufijos

-- 4

data T a = Em | N (T a) a (T a) deriving Show

altura :: T a -> Int
altura Em = 0
altura (N l x r) = 1 + max (altura l) (altura r)

t1 = N (N Em 1 Em) 8 (N (N Em 3 Em) 2 Em)
t2 = N (N (N Em 6 Em) 4 (N Em 7 Em)) 9 (N Em 5 Em)

combinar  Em t2 = t2
combinar t1 t2 = let (t1', mt1, _) = c t1
                 in N t1' mt1 t2
  where c (N Em a Em) = (Em, a, 0)
        c (N l a Em) = let (l', ml, hl) = c l
                       in (N l' a Em, ml, hl+1) 
        c (N Em a r) = let (r', mr, hr) = c r
                       in (N r' a Em, mr, hr+1)
        c (N l a r) = let ((l', ml, hl), (r', mr, hr)) = c l ||| c r
                      in if hl >= hr then (N l' a r, ml, hl+1) else (N l a r', mr, hr+1)
filterT :: (a -> Bool) -> T a -> T a
filterT p Em = Em
filterT p (N l a r) = let (l', r') = filterT p l ||| filterT p r
                      in if p a then N l' a r' else combinar l' r'
-- Worst case is O(n^3)
-- Best case is O(lg^3 n) ?
quicksort Em = Em
quicksort (N l a r) = let t = combinar l r
                          (l', r') = filterT (a >=) t ||| filterT (a <) t
                      in N (quicksort l') a (quicksort r')

-- 5

splitAtBT Empty i = (Empty, Empty)
splitAtBT (Node n l a r) i | (i - 1) == n = (Node n l a Empty, r)
                           | (i - 1) < n = let (l', r') = splitAtBT l i
                                           in (l', Node (d r' r) r' a r)
                           | otherwise = let (l', r') = splitAtBT r (i - n - 1)
                                         in (Node (d l l') l a l', r')
                           where d Empty Empty = 0
                                 d Empty (Node n _ _ _) = n
                                 d (Node n _ _ _) Empty = n
                                 d (Node n _ _ _) (Node n' _ _ _) = max n n'

balance Empty = Empty
balance (Node n l a r) = let (l', r') = b l ||| b r
                         in b (Node n l' a r')
  where 
    rank Empty = -1
    rank (Node n _ _ _) = n
    rr (Node _ (Node _ l y c) z d) = Node (1 + (max nn $ rank l)) l y (Node nn c z d)
                                   where nn = 1 + (max (rank c) (rank d))
    lr (Node _ a x (Node _ b y r)) = Node (1 + (max nn $ rank r)) (Node nn a x b) y r
                                   where nn = 1 + (max (rank a) (rank b))
    b Empty = Empty
    b n@(Node 0 Empty a Empty) = n
    b n@(Node _ (Node ny tx y c) z d) | (ny + 1 > rank d) && (rank tx + 1) > rank c = rr n
    b n@(Node _ a x (Node ny b y tz)) | (ny + 1 > rank a) && (rank tz + 1) > rank b = lr n
    b (Node nz n@(Node nx a x ty) z d) | (nx + 1 > rank d) && (rank ty + 1) > rank a = rr (Node nz (lr n) z d)
    b (Node nx a x n@(Node nz ty z d)) | (nz + 1 > rank a) && (rank ty + 1) > rank d = lr (Node nz a x (rr n))
    b n@(Node n' l a r) = n
