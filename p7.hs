import Seq
import ListSeq
import Par
scanToSeq f (s, t) = tabulateS (\i -> if i == (len - 1) then f t else f (nthS s i)) len
                 where len = lengthS s + 1

promedios :: Seq s => s Int -> s Float
promedios s = let (s',t) = scanS (+) 0 s
              in tabulateS (\i -> if i == (len - 1) then (fromIntegral t / fromIntegral len) else ( fromIntegral (nthS s' (i+1)) / fromIntegral (i + 1))) len
              where len = lengthS s


mayores s = (\(i, _, _) -> i-1) $ reduceS g (0, minBound, maxBound) (dropS (scanToSeq f (scanS max minBound s) :: [(Int, Int, Int)]) 1)
          where f i = (0,i,i)
                g (a,b,c) (a',b',c') | b < b' && b < c' = (a+a'+1, b', c)
                                     | b < b' = (a+a', b', c)
                                     | otherwise = (a, b, c)
-- 2

fibSeq n = scanS f (1,1,1,0) $ tabulateS (\i -> (1,1,1,0)) (n-1)
  where f (a,b,c,d) (a',b',c',d') = (a*a'+b*c', a*b'+b*d', c*a'+d*c', c*b'+d*d')

-- 3

aguaTest = fromList [2,3,4,7,5,2,3,2,6,4,3,5,2,1] :: [Int]

reverseS s = tabulateS (\i -> nthS s (len-i-1)) len
           where len = lengthS s

maxSA s = dropS (scanToSeq id $ scanS max minBound s) 1

aguaHist s = let (maxL, maxR) = maxSA s ||| (maxSA $ (reverseS s :: [Int]))
             in reduceS (+) 0 $ (tabulateS (f maxL maxR s) len :: [Int])
             where f :: [Int] -> [Int] -> [Int] -> Int -> Int
                   f maxL maxR s i = (min (nthS maxL i) (nthS maxR (len-i-1))) - (nthS s i)
                   len = lengthS s

-- 4

data Paren = Open | Close deriving Show

matchP s = case showtS s of
           EMPTY -> (0,0)
           ELT Open -> (0,1)
           ELT Close -> (1,0)
           NODE l r -> f (matchP l) (matchP r)
           where f (a,b) (a',b') | b == a' = (a, b')
                                 | b > a' = (a,b'+b-a')
                                 | otherwise = (a+a'-b, b')
matchParen s = matchP s == (0,0)

matchParenScan s = (0,0) == (snd $ scanS f (0,0) $ map g s)
                 where g Open = (0,1)
                       g Close = (1,0)
                       f (a,b) (a',b') | b == a' = (a, b')
                                       | b > a' = (a,b'+b-a')
                                       | otherwise = (a+a'-b, b')
-- 5
sccmlDC :: Seq s => s Int -> Int
sccmlDC s = let (m', p',s',t') = g $ (tabulateS h (lengthS s) :: [Int])
            in m'
  where h 0 = 0
        h i | nthS s i > nthS s (i-1) = 1
            | otherwise = minBound
        g s =case showtS s of
            EMPTY -> (0,0,0,0)
            ELT x -> let v' = max x 0
                     in (v',v',v', x)
            NODE l r -> let (l', r') = g l ||| g r
                        in f l' r'
                        where f (m,p,s,t) (m',p',s',t') = (max (s+p') (max m m'),  max p (t+p'), max s' (s+t'), t+t')

sccmlSA s = scanS f (minBound,minBound,0) s
             where f (l,r,t) (l',r',t') | r < l' = (l,r',t+t'+1)
                                        | otherwise = (l',r',t')
sccmlS s = (\(_,_,i) -> if i > 0 then i-1 else 0) $ reduceS (\(a,b,m) (a',b',m') -> if m > m' then (a,b,m) else (a',b',m')) (0,0,0) $ fst $ sccmlSA (tabulateS f (lengthS s):: [(Int,Int,Int)])
         where f i = let v = nthS s i
                     in (v, v, 0)

-- 7

merge cmp s s' = case showtS s of
                 EMPTY -> s'
                 ELT x -> let (l, r) = filterS (\i -> cmp i x == LT) s' ||| filterS (\i -> cmp i x /= LT) s'
                          in appendS (appendS l $ singletonS x) r
                 NODE l r -> let x = nthS r 0
                                 (l', r') = filterS (\i -> cmp i x == LT) s' ||| filterS (\i -> cmp i x /= LT) s'
                                 (l'', r'') = merge cmp l l' ||| merge cmp r r'
                             in appendS l'' r''

sort cmp s = case showtS s of
             NODE l r -> let (l', r') = sort cmp l ||| sort cmp r
                         in merge cmp l' r'
             _ -> s

maxE cmp s = snd $ scanS (\a b -> if cmp a b == LT then b else a) (nthS s 0) (dropS s 1)

maxS cmp s = snd $ scanS (\i j -> if cmp (nthS s i) (nthS s j) == LT then j else i) 0 (tabulateS (1+) (lengthS s -1) :: [Int])

group :: Seq s => (a -> a -> Ordering) -> s a -> s a
group cmp s = mapS (nthS s) $ filterS (\i -> if i == (len - 1) then True else cmp (nthS s i) (nthS s (i+1)) /= EQ) (tabulateS id len)
  where len = lengthS s

collectA :: (Seq s, Ord a) => s (a,b) -> s (a, s b)
collectA s = mapS (\(k,v)-> (k, singletonS v)) (sort (\(a,b) (a',b') -> compare a a') s)

collect s = let s' = collectA s
                aa = scanS f (nthS s' 0) (dropS s' 1)
            in group (\(a,b) (a',b') -> compare a a') ( appendS (fst aa ) (singletonS $ snd aa))
          where f (a,b) (a',b') | a == a' = (a, appendS b b')
                                | otherwise = (a', b')

mapCollectReduce apv red s = let pairs = joinS (mapS apv s)
                                 groups = collect pairs
                             in map red groups

estudiantes :: [([Char], [Int])]
estudiantes = [("juan",[1,2,3,4]),("pedro",[1,2,3,4]),("julian",[100,70,80,70]),("julian",[80,70,80,75]),("julian",[50,60,50,60])]

apv :: Seq s => (a , s Int) -> [([Char], Int)]
apv (_,notas) = singletonS (cond prom, maxE compare notas)
          where prom = div (reduceS (+) 0 notas) (lengthS notas)
                cond p | p >= 70 = "aprobado"
                       | p >= 50 = "espera"
                       | otherwise = "zzz"
red :: Seq s => ([Char], s Int) -> ([Char], Int, Int)
red (w,s) = (w, lengthS s, maxE compare s)


datosIngreso s =  mapCollectReduce apv red s

-- 9

apv9a x = map (\i -> (i,1)) x

red9a (w,s) = (w,reduceS (+) 0 s)

countCaract s = mapCollectReduce apv9a red9a s

huffmax s = collect $ mapS (\(a,b) -> (b,a)) $ countCaract s
