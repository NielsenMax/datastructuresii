module ListSeq where

import Par
import Seq

contractL _ [] = []
contractL _ [x] = [x]
contractL f (x : y : xs) = let (x', xs') = f x y ||| contractL f xs
                          in x' : xs'

expandL _ [] _ = []
expandL _ [x] ys = ys
expandL f (x:_:xs) (y:ys) = let (x', xs') = f y x ||| expandL f xs ys
                            in y:x':xs'

instance Seq [] where
  emptyS = []
  singletonS = (: emptyS)
  lengthS = length
  nthS = (!!)
  tabulateS f n = tabulateS' f n 0
                  where tabulateS' f n i | n == i = []
                                         | otherwise = let (x, xs) = f i ||| tabulateS' f n (i+1)
                                                    in x : xs
  mapS _ [] = []
  mapS f (x : xs) = let (x', xs') = f x ||| mapS f xs
                    in x' : xs'
  filterS _ [] = []
  filterS f (x : xs) = let (b, xs') = f x ||| filterS f xs
                       in if b then x : xs' else xs'
  appendS [] ys = ys
  appendS (x : xs) ys = x : appendS xs ys
  takeS xs n = take n xs
  dropS xs n = drop n xs
  showtS [] = EMPTY
  showtS [x] = ELT x
  showtS xs = let (ls, rs) = split xs $ div (lengthS xs) 2
              in NODE ls rs
              where split [] _ = ([], [])
                    split xs 0 = ([], xs)
                    split (x:xs) n = let (ls, rs) = split xs $ n-1
                                     in (x:ls, rs)
  showlS [] = NIL
  showlS (x : xs) = CONS x xs
  reduceS f b [] = b 
  reduceS f b [x] = f b x
  reduceS f b xs = reduceS f b $ contractL f xs
  joinS xss = reduceS appendS [] xss
  scanS f b [] = ([], b)
  scanS f b [x] = ([b], f b x)
  scanS f b xs = let (s', t) = scanS f b $ contractL f xs
                 in (expandL f xs s', t)
  fromList = id
