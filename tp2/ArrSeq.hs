module ArrSeq where

import Par
import Seq
import qualified Arr as A

isEmpty = (0==) . A.length
isSingleton = (1==) . A.length

hd = flip (A.!) 0 
tl xs = A.subArray 1 (A.length xs - 1) xs 

contractA :: (a -> a -> a) -> A.Arr a -> A.Arr a
contractA f xs | even xn = tabulateS g mid
               | otherwise = tabulateS h $ mid + 1
               where xn = lengthS xs
                     mid = div xn 2
                     g i = f (nthS xs (2*i)) (nthS xs (2*i+1))
                     h i = if i == mid then nthS xs (2*i) else g i

expandA f s s' = tabulateS (\i -> if even i then g i else f (g i) (nthS s (i-1))) (lengthS s)
               where g i = nthS s' $ div i 2
instance Seq A.Arr where
  emptyS = A.empty
  singletonS x = A.tabulate (\a -> x) 1
  lengthS = A.length
  nthS = (A.!)
  tabulateS = A.tabulate
  takeS xs i = A.subArray 0 i xs
  dropS xs i = A.subArray (i) (lengthS xs - i ) xs
  joinS = A.flatten
  showtS xs | isEmpty xs = EMPTY
            | isSingleton xs = ELT (nthS xs 0)
            | otherwise = NODE (takeS xs (div (lengthS xs) 2)) (dropS xs (div (lengthS xs) 2))
  showlS xs | isEmpty xs = NIL
            | otherwise = CONS (nthS xs 0) (dropS xs 1)
  appendS xs ys = tabulateS (f xs ys) (xn + yn)
                where xn = lengthS xs 
                      yn = lengthS ys
                      f xs ys i | i < xn = nthS xs i
                                | otherwise = nthS ys (i - xn)
  mapS f xs = tabulateS (f . nthS xs) (lengthS xs)                        
  filterS f xs = joinS $ mapS (\x -> if f x then singletonS x else emptyS) xs
  reduceS f b xs | isEmpty xs = b
                 | isSingleton xs = f b $ hd xs
                 | otherwise = reduceS f b $ contractA f xs
  scanS f b xs | isEmpty xs = (emptyS, b)
               | isSingleton xs = (singletonS b, f b (hd xs))
               | otherwise = let (s', t) = scanS f b $ contractA f xs
                             in (expandA f xs s', t)
  fromList = A.fromList

